various button and toggle styles for SwiftUI

![screenshot 1][1]
![screenshot 2][2]
![screenshot 3][3]
![screenshot 4][4]

[1]: screenshot01.png
[2]: screenshot02.png
[3]: screenshot03.png
[4]: screenshot04.png
