//
//  GradientBackground.swift
//  ButtonAndToggleStyles
//
//  Created by vikingosegundo on 25.12.23.
//

import SwiftUI

struct AnimatedPositionLinearGradientBackground: View {
    @State private var animateGradient = false
    @State var gradientColors: [Color] = [.purple,.mint, .yellow]
    @State var animationDuration:CGFloat = 20
    @State var animationAutoreverses = true

    var body: some View {
        LinearGradient(colors: gradientColors, startPoint: animateGradient ? .topLeading : .bottomLeading, endPoint: animateGradient ? .bottomTrailing : .topTrailing)
            .ignoresSafeArea()
            .onAppear {
                withAnimation(.linear(duration: animationDuration).repeatForever(autoreverses: animationAutoreverses)) {
                    animateGradient.toggle()
                }
            }
    }
}

struct AnimatedHueRotationLinearGradientBackground: View {
    @State private var animateGradient = false
    @State var gradientColors: [Color] = [.purple,.mint, .yellow]
    @State var animationDuration:CGFloat = 5

    var body: some View {
        LinearGradient(colors: gradientColors, startPoint: .topLeading, endPoint: .bottomTrailing)
            .hueRotation(.degrees(animateGradient ? 360 : 0))
            .ignoresSafeArea()
            .onAppear {
                    withAnimation(.easeInOut(duration: animationDuration).repeatForever(autoreverses: true)) {
                        animateGradient.toggle()
                    }
                }
    }
}


struct RadialGradientBackground: View {
    @State private var animateGradient = false
    @State var gradientColors: [Color] = [.purple,.mint, .yellow]
    @State var animationDuration:CGFloat = 2.0
    @State var animationStartRadius:CGFloat = 400
    @State var animationEndRadius:CGFloat = 20

    @State var animationCenterPoint:UnitPoint = .center
    @State var animationAutoreverses = true

    var body: some View {
        RadialGradient(colors: gradientColors, center: animationCenterPoint, startRadius: animateGradient ? animationStartRadius : animationStartRadius/2, endRadius: animateGradient ? animationEndRadius : animationEndRadius*2)
            .ignoresSafeArea()
            .onAppear {
                withAnimation(.linear(duration: animationDuration).repeatForever(autoreverses: animationAutoreverses)) {
                    animateGradient.toggle()
                }
            }
    }
}

struct AnimatableGradientModifier: AnimatableModifier {
    let fromGradient: Gradient
    let toGradient: Gradient
    var progress: CGFloat = 0.0
 
    var animatableData: CGFloat {
        get { progress }
        set { progress = newValue }
    }
 
    func body(content: Content) -> some View {
        var gradientColors = [Color]()
 
        for i in 0..<fromGradient.stops.count {
            let fromColor = UIColor(fromGradient.stops[i].color)
            let toColor = UIColor(toGradient.stops[i].color)
 
            gradientColors.append(colorMixer(fromColor: fromColor, toColor: toColor, progress: progress))
        }
 
        return LinearGradient(gradient: Gradient(colors: gradientColors), startPoint: .topLeading, endPoint: .bottomTrailing)
    }
 
    func colorMixer(fromColor: UIColor, toColor: UIColor, progress: CGFloat) -> Color {
        guard let fromColor = fromColor.cgColor.components else { return Color(fromColor) }
        guard let toColor = toColor.cgColor.components else { return Color(toColor) }
 
        let red = fromColor[0] + (toColor[0] - fromColor[0]) * progress
        let green = fromColor[1] + (toColor[1] - fromColor[1]) * progress
        let blue = fromColor[2] + (toColor[2] - fromColor[2]) * progress
 
        return Color(red: Double(red), green: Double(green), blue: Double(blue))
    }
}
extension View {
    func animatableGradient(fromGradient: Gradient, toGradient: Gradient, progress: CGFloat) -> some View {
        self.modifier(AnimatableGradientModifier(fromGradient: fromGradient, toGradient: toGradient, progress: progress))
    }
}

struct AnimatedGradientMixerBackground:View {
    @State private var progress: CGFloat = 0
    let gradient1 = Gradient(colors: [.purple, .mint,.yellow])
    let gradient2 = Gradient(colors: [.blue, .orange,.purple])
    
    var body: some View {
        Rectangle()
            .animatableGradient(fromGradient: gradient1, toGradient: gradient2, progress: progress)
            .ignoresSafeArea()
            .onAppear {
                withAnimation(.linear(duration: 5.0).repeatForever(autoreverses: true)) {
                    self.progress = 1.0
                }
            }
    }
}
