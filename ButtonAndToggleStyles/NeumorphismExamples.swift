//
//  NeumorphismButtons.swift
//  ButtonAndToggleStyles
//
//  Created by vikingosegundo on 18.12.23.
//

import SwiftUI

extension Color {
    static let bgGradient = [
        Color(red: 60 / 255, green: 60 / 255, blue: 60 / 255),
        Color(red: 25 / 255, green: 25 / 255, blue: 30 / 255)
    ]
}

extension LinearGradient {
    init(_ colors: Color...) {
        self.init(gradient: Gradient(colors: colors), startPoint: .topLeading, endPoint: .bottomTrailing)
    }
}

extension AngularGradient {
    init(_ colors:Color...) {
        self.init(colors: colors, center: .center)
    }
}

struct GradientBackground<S: Shape>: View {
    var isHighlighted: Bool
    var shape: S

    var body: some View {
        ZStack {
            if isHighlighted {
                shape
                    .fill(LinearGradient(.purple, .teal))
                    .overlay(shape.stroke(AngularGradient(.teal,.purple,.teal), lineWidth: 4))
                    .shadow(color: .bgGradient.first!, radius: 10, x: 5, y: 5)
                    .shadow(color: .bgGradient.last!, radius: 10, x: -5, y: -5)
            } else {
                shape
                    .fill(LinearGradient(.bgGradient.first!, .bgGradient.last!))
                    .overlay(shape.stroke(AngularGradient(.teal,.purple,.teal), lineWidth: 4))
                    .shadow(color: .bgGradient.first!, radius: 5, x: -5, y: -5)
                    .shadow(color: .bgGradient.last!, radius: 5, x: 5, y: 5)
            }
        }
    }
}

struct CapsuleGradientButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(minWidth: 44)
            .frame(height: 44)
            .padding(16)
            .contentShape(Capsule())
            .foregroundColor(.white)
            .multilineTextAlignment(.center)
            .background(
                GradientBackground(isHighlighted: configuration.isPressed, shape: Capsule())
            )
    }
}

struct RoundedRectangleGradientButtonStyle: ButtonStyle {
    var cornerSize: CGSize = CGSize(width: 10, height: 10)

    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(minWidth: 44)
            .frame(height: 44)
            .padding(16)
            .contentShape(RoundedRectangle(cornerSize: cornerSize))
            .foregroundColor(.white)
            .multilineTextAlignment(.center)
            .background(
                GradientBackground(isHighlighted: configuration.isPressed, shape: RoundedRectangle(cornerSize: cornerSize))
            )
    }
}

struct CapsuleGradientToggleStyle: ToggleStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        Button{
            configuration.isOn.toggle()
        } label: {
            configuration.label
                .frame(minWidth: 44)
                .frame(height: 44)
                .padding(16)
                .foregroundColor(.white)
                .contentShape(Capsule())
        }
        .background(
            GradientBackground(isHighlighted: configuration.isOn, shape: Capsule())
        )
    }
}

struct RoundedRectangleGradientToggleStyle: ToggleStyle {
    var cornerSize:CGSize = CGSize(width: 10, height: 10)

    func makeBody(configuration: Self.Configuration) -> some View {
        Button(action: {
            configuration.isOn.toggle()
        }) {
            configuration.label
                .frame(minWidth: 44)
                .frame(height: 44)
                .padding(16)
                .foregroundColor(.white)
                .contentShape(RoundedRectangle(cornerSize: cornerSize))
        }
        .background(
            GradientBackground(isHighlighted: configuration.isOn, shape: RoundedRectangle(cornerSize: cornerSize))
        )
    }
}
struct NeumorphismExamples:View {
    @State private var isToggled0 = false
    @State private var isToggled1 = false
    @State private var isToggled2 = false
    @State private var isToggled3 = false
    @State private var isToggled4 = false

    var body: some View {
        ZStack {
            LinearGradient(.bgGradient.first!, .bgGradient.last!)
                .edgesIgnoringSafeArea(.all)
            ScrollView {
                VStack {
                    HStack(spacing: 25) {
                        Button(action: {
                            print("Button tapped")
                        }) {
                            Image(systemName: "heart.fill")
                        }
                        .buttonStyle(CapsuleGradientButtonStyle())
                        
                        Button(action: {
                            print("Button tapped")
                        }) {
                            Text("press")
                        }
                        .buttonStyle(CapsuleGradientButtonStyle())
                        
                        Toggle(isOn: $isToggled0) {
                            Image(systemName: "camera.fill")
                        }.toggleStyle(CapsuleGradientToggleStyle())
                        
                        Toggle(isOn: $isToggled1) {
                            Text("toggle")
                        }
                        .toggleStyle(CapsuleGradientToggleStyle())
                    }.padding()
                    HStack(spacing: 25) {
                        Button(action: {
                            print("Button tapped")
                        }) {
                            Text("a potentially looooooooong button title")
                        }
                        .buttonStyle(CapsuleGradientButtonStyle())
                        Toggle(isOn: $isToggled2) {
                            Text("a potentially looooooooong toggle title")
                        }
                        .toggleStyle(CapsuleGradientToggleStyle())                        
                    }.padding()
                    HStack(spacing:25) {
                        Button(action: {
                            print("Button tapped")
                        }) {
                            Image(systemName: "heart.fill")
                        }
                        .buttonStyle(RoundedRectangleGradientButtonStyle())
                        
                        Button(action: {
                            print("Button tapped")
                        }) {
                            Text("press this button")
                        }
                        .buttonStyle(RoundedRectangleGradientButtonStyle(cornerSize: CGSize(width: 25, height: 40)))
                        Toggle(isOn: $isToggled3) {
                            Image(systemName: "camera.fill")
                        }.toggleStyle(RoundedRectangleGradientToggleStyle())
                        
                    }.padding()
                    HStack(spacing: 40) {
                        Button(action: {
                            print("Button tapped")
                        }) {
                            Text("a potentially looooooooong button title")
                        }
                        .buttonStyle(RoundedRectangleGradientButtonStyle(cornerSize: CGSize(width: 25, height: 40)))
                        Toggle(isOn: $isToggled4) {
                            Text("a potentially looooooooong toggle title")
                        }
                        .toggleStyle(RoundedRectangleGradientToggleStyle(cornerSize: CGSize(width: 25, height: 40)))
                    }.padding()
                }
            }
        }
    }
}

#Preview {
    NeumorphismExamples()
}
