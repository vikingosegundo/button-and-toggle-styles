//
//  ButtonAndToggleStylesApp.swift
//  ButtonAndToggleStyles
//
//  Created by vikingosegundo on 18.12.23.
//

import SwiftUI

@main
struct ButtonAndToggleStylesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
