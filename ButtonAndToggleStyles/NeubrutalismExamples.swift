//
//  NeubrutalismExamples.swift
//  ButtonAndToggleStyles
//
//  Created by vikingosegundo on 02.01.24.
//

import SwiftUI

struct BrutalismBackground<S: Shape>: View {
    var shape: S
    var isEnabled:Bool = true
    var isPressed:Bool
    var shadowColor:Color
    var backgroundColor:Color
    var borderColor: Color
    var strokeWidth:CGFloat
    var enabledShadowWidth:CGFloat
    var disabledShadowWidth:CGFloat

    var body: some View {
        ZStack {
            if isEnabled {
                shape
                    .fill(backgroundColor)
                    .shadow(color: shadowColor, radius: 0, x: enabledShadowWidth, y: enabledShadowWidth)
                    .overlay(shape.stroke(borderColor, lineWidth: strokeWidth))
            } else {
                shape
                    .fill(.gray)
                    .shadow(color: shadowColor, radius: 0, x: disabledShadowWidth, y: disabledShadowWidth)
                    .overlay(shape.stroke(borderColor.opacity(0.5), lineWidth: strokeWidth))
            }
        }
    }
}

struct RoundedRectangleBrutalismButtonStyle: ButtonStyle {
    @Environment(\.isEnabled) var isEnabled

    var cornerSize: CGSize = CGSize(width: 10, height: 10)
    var borderWidth:CGFloat = 4
    var foregroundColor:Color = .buttonTitle
    var backgroundColor:Color = .buttonBackground
    var borderColor:Color = .buttonBorder
    var strokeWidth:CGFloat = 4
    var onShadowWidth:CGFloat = 0
    var offShadowWidth:CGFloat = 3
    var disabledShadowWidth:CGFloat = 0
    var shadowColor:Color = .buttonShadow

    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .contentShape(RoundedRectangle(cornerSize: cornerSize))
            .frame(minWidth: 36, minHeight: 44)
            .padding(.horizontal, 4)
            .background(
                BrutalismBackground(
                    shape: RoundedRectangle(cornerSize: cornerSize),
                    isEnabled: isEnabled,
                    isPressed: configuration.isPressed,
                    shadowColor: shadowColor,
                    backgroundColor: backgroundColor,
                    borderColor: borderColor,
                    strokeWidth: strokeWidth,
                    enabledShadowWidth: configuration.isPressed ? onShadowWidth : offShadowWidth,
                    disabledShadowWidth: disabledShadowWidth
                )
            )
            .foregroundColor(isEnabled ? foregroundColor : .black.opacity(0.5))
            .offset(.init(width: configuration.isPressed ? 0 : -2, height: configuration.isPressed ? 0 : -2))
    }
}

struct RoundedRectangleBrutalismToggleStyle: ToggleStyle {
    var cornerSize:CGSize = CGSize(width: 10, height: 10)
    var foregroundColor:Color = .buttonTitle
    var backgroundColor:Color = .buttonBackground
    var borderColor:Color = .buttonBorder
    var strokeWidth:CGFloat = 4
    var onShadowWidth:CGFloat = 4
    var offShadowWidth:CGFloat = 6
    var disabledShadowWidth:CGFloat = 0
    var shadowColor:Color = .buttonShadow
    var onIcon = Image(systemName: "checkmark.square")
    var offIcon = Image(systemName: "square")
    
    private func icon(isOn:Bool) -> Image {
        isOn
            ? onIcon
            : offIcon
    }

    func makeBody(configuration: Self.Configuration) -> some View {
        HStack {
            icon(isOn: configuration.isOn)
                .padding(.leading, 4)
            configuration.label
                .padding(.trailing, 4)
        }
        .frame(minWidth: 44,minHeight: 44)
        .foregroundColor(foregroundColor)
        .padding(.horizontal, 5)
        .contentShape(RoundedRectangle(cornerSize: cornerSize))
        .background(
            BrutalismBackground(
                shape: RoundedRectangle(cornerRadius: cornerSize.width),
                isPressed: configuration.isOn,
                shadowColor: shadowColor,
                backgroundColor: backgroundColor,
                borderColor: borderColor,
                strokeWidth: strokeWidth,
                enabledShadowWidth: configuration.isOn ? onShadowWidth : offShadowWidth, 
                disabledShadowWidth: disabledShadowWidth
            )
        ).onTapGesture {
            configuration.isOn.toggle()
        }
    }
}

struct CheckboxBrutalismToggleStyle: ToggleStyle {
    var foregroundColor:Color = .buttonTitle
    var onBackgroundColor:Color = .toggleOn
    var offBackgroundColor:Color = .toggleOff
    var shadowColor: Color = .buttonShadow
    var onShadowWidth: CGFloat = 0
    var offShadowWidth: CGFloat = 3
    var disabledShadowWidth:CGFloat = 0
    var checkboxBorderWidth:CGFloat = 3
    var checkboxCornerRadius:CGFloat = 3
        
    func makeBody(configuration: Self.Configuration) -> some View {
            HStack {
                ZStack {
                    RoundedRectangle(cornerRadius: checkboxCornerRadius)
                        .stroke(foregroundColor, lineWidth: checkboxBorderWidth)
                        .frame(width: 24, height: 24, alignment: .center)
                    if configuration.isOn {
                       Image(systemName: "checkmark")
                            .resizable()
                           .frame(width: 16, height: 16, alignment: .center)

                    }
                }.background(
                    BrutalismBackground(
                        shape: RoundedRectangle(cornerRadius: checkboxCornerRadius),
                        isPressed: configuration.isOn,
                        shadowColor: shadowColor,
                        backgroundColor: configuration.isOn ? onBackgroundColor : offBackgroundColor,
                        borderColor: .black,
                        strokeWidth: checkboxBorderWidth,
                        enabledShadowWidth: configuration.isOn ? onShadowWidth : offShadowWidth,
                        disabledShadowWidth: disabledShadowWidth)
                )
            configuration.label
        }
        .padding(.horizontal, 2)
        .foregroundColor(foregroundColor)
        .onTapGesture {
            configuration.isOn.toggle()
        }
    }
}

struct NeubrutalismExamples:View {
    @Environment(\.colorScheme) var colorScheme

    @State private var toggled0:Bool = false
    @State private var toggled1:Bool = false
    @State private var toggled2:Bool = true
    @State private var toggled3:Bool = false
    @State private var toggled4:Bool = true
    @State private var toggled5:Bool = false
    @State private var toggled6:Bool = false
    @State private var toggled7:Bool = true

    func isDark() -> Bool { colorScheme == .dark }

    var body: some View {
        VStack {
            ScrollView {
                HStack {
                    Button {
                        print("Button tapped")
                    } label: {
                        Image(systemName: "heart.fill")
                    }
                    .buttonStyle(
                        RoundedRectangleBrutalismButtonStyle(
                            backgroundColor: .yellow,
                            strokeWidth: 3)
                    )

                    Button {
                        print("Button tapped")
                    } label: {
                        Image(systemName: "camera.fill")
                    }
                    .buttonStyle(
                        RoundedRectangleBrutalismButtonStyle(backgroundColor: .teal)
                    )
                    
                    Button {
                        print("Button tapped")
                    } label: {
                        Text("Button")
                    }
                    .buttonStyle(
                        RoundedRectangleBrutalismButtonStyle(backgroundColor: .orange)
                    )
                    
                    Button {
                        print("Button tapped")
                    } label: {
                        Image(systemName: "camera.fill")
                    }
                    .buttonStyle(
                        RoundedRectangleBrutalismButtonStyle(
                            cornerSize: .zero,
                            backgroundColor: .red,
                            offShadowWidth:6,
                            shadowColor:isDark() ? .white.opacity(0.25) : .gray)
                    )
                }.padding()
                HStack {
                    Button {
                        print("Button tapped")
                    } label: {
                        Image(systemName: "camera.fill")
                    }
                    .buttonStyle(
                        RoundedRectangleBrutalismButtonStyle(
                            backgroundColor: .red,
                            shadowColor:.gray
                        )
                    )
                    .disabled(true)
                }
                HStack(spacing: 10) {
                    Toggle("Toggle 0", isOn: $toggled0)
                        .toggleStyle(
                            RoundedRectangleBrutalismToggleStyle(
                                backgroundColor: .teal,
                                shadowColor: isDark() ? .white.opacity(0.25) : .gray
                            )
                        )
                    Toggle("Toggle 1", isOn: $toggled1)
                        .toggleStyle(
                            RoundedRectangleBrutalismToggleStyle(
                                backgroundColor: .teal
                            )
                        )
                    Toggle("Toggle 2", isOn: $toggled2)
                        .toggleStyle(
                            RoundedRectangleBrutalismToggleStyle()
                        )
                }.padding()
                VStack {
                    HStack {
                        Toggle("Toggle 3", isOn: $toggled3)
                            .toggleStyle(
                                CheckboxBrutalismToggleStyle()
                            )
                        Toggle("Toggle 4", isOn: $toggled4)
                            .toggleStyle(
                                CheckboxBrutalismToggleStyle(
                                    onBackgroundColor: .yellow,
                                    offBackgroundColor:.clear
                                )
                            )
                    }
                    HStack {
                        Toggle("Toggle 5", isOn: $toggled5)
                            .toggleStyle(
                                CheckboxBrutalismToggleStyle(
                                    onBackgroundColor: .teal,
                                    offBackgroundColor: .teal,
                                    shadowColor: .gray
                                )
                            )
                        Toggle("Toggle 6", isOn: $toggled6)
                            .toggleStyle(
                                CheckboxBrutalismToggleStyle(
                                    onBackgroundColor: .teal,
                                    offBackgroundColor: .teal,
                                    shadowColor: .gray,
                                    onShadowWidth:3,
                                    offShadowWidth:5,
                                    checkboxCornerRadius:7
                                )
                            )
                    }
                    HStack {
                        Toggle("Toggle 7", isOn: $toggled7)
                            .toggleStyle(
                                CheckboxBrutalismToggleStyle(
                                    checkboxCornerRadius:0
                                )
                            )
                    }
                }.padding()
            }
        }
    }
}

#Preview {
    NeubrutalismExamples()
}
