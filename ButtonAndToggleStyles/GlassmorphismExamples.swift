//
//  GlassmorphismButtons.swift
//  ButtonAndToggleStyles
//
//  Created by vikingosegundo on 18.12.23.
//

import SwiftUI
import Combine

struct FrostedGlassBackground<S: Shape>: View {
    var shape: S
    var isPressed:Bool
    var isEnabled:Bool = true
    var body: some View {
        ZStack {
            if isEnabled {
                shape
                    .fill(isPressed ? .thinMaterial : .ultraThinMaterial )
            } else {
                shape
                    .fill(.gray.opacity(0.5))
            }
        }
    }
}

struct CapsuleFrostedGlassButtonStyle: ButtonStyle {
    @Environment(\.isEnabled) var isEnabled

    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .foregroundStyle(.secondary)
            .padding(30)
            .contentShape(Capsule())
            .background(
                FrostedGlassBackground(
                    shape: Capsule(),
                    isPressed: configuration.isPressed,
                    isEnabled: isEnabled)
            )
            .foregroundColor(.secondary)
    }
}

struct CapsuleFrostedGlassToggleStyle: ToggleStyle {
    @Environment(\.isEnabled) var isEnabled
    var onColor: Color = .blue
    var offColor: Color = .gray
    var textColor: Color = .secondary
    var iconSize: CGFloat = 22
    var onIcon = Image(systemName: "checkmark.circle")
    var offIcon = Image(systemName: "circle")
    
    private func icon(isOn:Bool) -> Image {
        isOn 
            ? onIcon
            : offIcon
    }
    
    func makeBody(configuration: Configuration) -> some View {
        HStack {
            configuration.label
                .foregroundStyle(.secondary)
            Spacer()
            icon(isOn: configuration.isOn)
                .resizable()
                .foregroundColor(configuration.isOn ? onColor : offColor)
                .frame(width: iconSize, height: iconSize)
                .offset(x: -5)
        }
        .frame(minWidth: 44)
        .frame(height: 44)
        .padding(16)
        .foregroundColor(textColor)
        .contentShape(Capsule())
        .background(
            FrostedGlassBackground(
                shape: Capsule(),
                isPressed: configuration.isOn,
                isEnabled: isEnabled)
        )
        .onTapGesture {
            configuration.isOn.toggle()
        }
    }
}

struct RoundedRectangleFrostedGlassToggleStyle: ToggleStyle {
    @Environment(\.isEnabled) var isEnabled
    var onColor: Color = .blue
    var offColor: Color = .gray
    var textColor: Color = .secondary
    var iconSize: CGFloat = 22
    var roundedCorners: CGSize = CGSize(width: 10, height: 10)
    var onIcon = Image(systemName: "checkmark.circle")
    var offIcon = Image(systemName: "circle")
    
    private func icon(isOn:Bool) -> Image {
        isOn
            ? onIcon
            : offIcon
    }
    
    func makeBody(configuration: Configuration) -> some View {
        HStack {
            configuration.label
                .foregroundStyle(.secondary)
            Spacer()
            icon(isOn: configuration.isOn)
                .resizable()
                .foregroundColor(configuration.isOn ? onColor : offColor)
                .frame(width: iconSize, height: iconSize)
                .offset(x: -5)
        }
        .frame(minWidth: 44)
        .frame(height: 44)
        .padding(16)
        .foregroundColor(textColor)
        .contentShape(RoundedRectangle(cornerSize: roundedCorners))
        .background(
            FrostedGlassBackground(
                shape:RoundedRectangle(cornerSize: roundedCorners),
                isPressed: configuration.isOn,
                isEnabled: isEnabled)
        )
        .onTapGesture {
            configuration.isOn.toggle()
        }
    }
}

struct CapsuleFrostedGlassTextFieldStyle: TextFieldStyle{
    @Environment(\.isEnabled) var isEnabled

    var roundedCornes: CGFloat = 6
    var textColor: Color = .secondary
    var focused = false

    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .padding()
            .background(
                FrostedGlassBackground(
                    shape: Capsule(),
                    isPressed: focused,
                    isEnabled: isEnabled)
            )
            .contentShape(Capsule())
            .cornerRadius(roundedCornes)
            .foregroundColor(textColor)
    }
}

struct GlassmorphismExamples: View {
    @State private var isToggled0 = false
    @State private var isToggled1 = true
    @State private var isToggled2 = false
    @State private var isToggled3 = false
    @State private var isToggled4 = true
    @State private var isToggled5 = false

    @State private var username = ""
    @State private var password = ""

    var body: some View {
        ZStack {
            BlurredBackground()
         // AnimatedPositionLinearGradientBackground()
         // RadialGradientBackground()
         // AnimatedHueRotationLinearGradientBackground()
         // AnimatedGradientMixerBackground()
            
            VStack {
                ScrollView {
                        VStack {
                            HStack {
                                Button {
                                    print("button clicked")
                                } label: {
                                    Text("Click me")
                                }
                                
                                Button {
                                    print("button clicked")
                                } label: {
                                    Image(systemName: "heart.fill")
                                }
                            }
                        }
                        VStack {
                            Toggle(isOn: $isToggled0) {
                                Text("Toggle 0")
                            }.toggleStyle(CapsuleFrostedGlassToggleStyle(onColor: .green))
                                .frame(maxWidth: 220)
                            
                            Toggle(isOn: $isToggled1) {
                                Text("a potentially extra looooooooong toggle 1 title")
                            }.toggleStyle(CapsuleFrostedGlassToggleStyle(onColor: .green))
                            
                            HStack {
                                Toggle(isOn: $isToggled2) {
                                    Text("Toggle 2")
                                }.toggleStyle(CapsuleFrostedGlassToggleStyle(
                                    onColor: .green,
                                    onIcon: Image(systemName: "checkmark.square"),
                                    offIcon: Image(systemName: "square"))
                                ).frame(maxWidth: 220)
                                
                                Toggle(isOn: $isToggled3) {
                                    Text("Toggle 3")
                                }.toggleStyle(CapsuleFrostedGlassToggleStyle(
                                    onColor: .red,
                                    offColor: .red,
                                    onIcon: Image(systemName: "checkmark.circle.fill"))
                                ).frame(maxWidth: 220)
                            }
                            HStack {
                                Toggle(isOn: $isToggled4) {
                                    Text("Toggle 4")
                                }.toggleStyle(RoundedRectangleFrostedGlassToggleStyle(
                                    roundedCorners: CGSize(width: 15, height: 15),
                                    onIcon: Image(systemName: "checkmark.circle.fill"))
                                ).frame(maxWidth: 220)
                                
                                Toggle(isOn: $isToggled5) {
                                    Text("Toggle 5")
                                }.toggleStyle(RoundedRectangleFrostedGlassToggleStyle(
                                    onColor: .green,
                                    offColor: .green,
                                    roundedCorners: CGSize(width: 15, height: 40),
                                    onIcon: Image(systemName: "checkmark.circle.fill"))
                                )
                                .frame(maxWidth: 220)
                                .disabled(
                                    username.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
                                 || password.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)
                            }
                            HStack {
                                CredentialsView(username: $username, password: $password)
                                Button {
                                    password = ""
                                    username = ""
                                } label: {
                                    Text("login")
                                }.disabled(
                                    username.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
                                 || password.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)
                            }
                        }
                    }
                }
                .padding()
                .buttonStyle(CapsuleFrostedGlassButtonStyle())
                .containerRelativeFrame(.horizontal)
        }
    }
}


struct CredentialsView: View {
    private enum Field: Hashable {
        case username
        case password
    }

    @Binding var username:String
    @Binding var password:String
    @FocusState private var focusedField: Field?

    var body: some View {
        VStack {
            Group {
                TextField("Username", text: $username)
                    .focused($focusedField, equals: .username)
                    .textFieldStyle(CapsuleFrostedGlassTextFieldStyle(focused: focusedField == .username))
                
                SecureField("Password", text: $password)
                    .focused($focusedField, equals: .password)
                    .textFieldStyle(CapsuleFrostedGlassTextFieldStyle(focused: focusedField == .password))
            }
        }
    }
}

#Preview {
    GlassmorphismExamples()
}
