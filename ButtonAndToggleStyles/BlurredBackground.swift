//
//  BlurredBackground.swift
//  ButtonAndToggleStyles
//
//  Created by vikingosegundo on 21.12.23.
//

import SwiftUI

struct BlurredBackground: View {
    struct AnimationProperties {
        var xTranslation = 0.0
        var yTranslation = 0.0
    }
    var body: some View {
        LinearGradient(colors: [.cyan.opacity(0.7), .purple.opacity(0.8)], startPoint: .topLeading, endPoint: .bottomTrailing).edgesIgnoringSafeArea(.all)
        ZStack {
            RoundedRectangle(cornerRadius: 10, style: .continuous)
                .frame(width: 300, height: 300)
                .foregroundStyle(LinearGradient(colors: [.indigo, .teal], startPoint: .top, endPoint: .leading))
                .offset(CGSize(width: -100, height: 100))
                .rotationEffect(.degrees(15))
                .blur(radius: 100)
                .keyframeAnimator(initialValue: AnimationProperties(), repeating: true) { content, value in
                    content
                        .offset(x:value.xTranslation,y: value.yTranslation)
                } keyframes : { _ in
                    KeyframeTrack(\.xTranslation) {
                        CubicKeyframe(0, duration: 2)
                        CubicKeyframe(-1*Double.random(in:(100..<400)), duration: 4)
                        CubicKeyframe(Double.random(in:(100..<400)), duration: 4)
                        CubicKeyframe(0, duration: 5.0)
                    }
                    KeyframeTrack(\.yTranslation) {
                        CubicKeyframe(0, duration: 2)
                        CubicKeyframe(-1*Double.random(in:(100..<400)), duration: 4)
                        CubicKeyframe(-1*Double.random(in:(100..<400)), duration: 4)
                        CubicKeyframe(0, duration: 5)
                    }
                }
            
            Circle()
                .frame(width: 300)
                .foregroundColor(.blue)
                .offset(x: -100, y: -150)
                .blur(radius: 100)
                .keyframeAnimator(initialValue: AnimationProperties(), repeating: true) { content, value in
                    content
                        .offset(x:value.xTranslation,y: value.yTranslation)
                } keyframes : { _ in
                    KeyframeTrack(\.xTranslation) {
                        CubicKeyframe(0, duration: 2)
                        CubicKeyframe(-1*Double.random(in:(50..<200)), duration: 4)
                        CubicKeyframe(Double.random(in:(50..<200)), duration:4)
                        CubicKeyframe(0, duration: 5)
                    }
                    KeyframeTrack(\.yTranslation) {
                        CubicKeyframe(0, duration: 2)
                        CubicKeyframe(-1*Double.random(in:(100..<200)), duration: 4)
                        CubicKeyframe(-1*Double.random(in:(100..<200)), duration: 4)
                        CubicKeyframe(0, duration: 5)
                    }
                }
            
           RoundedRectangle(cornerRadius: 30, style: .continuous)
               .frame(width: 500, height: 500)
               .foregroundStyle(LinearGradient(colors: [.purple, .mint], startPoint: .top, endPoint: .leading))
               .offset(x: 300)
               .rotationEffect(.degrees(30))
               .blur(radius: 100)
               .keyframeAnimator(initialValue: AnimationProperties(), repeating: true) { content, value in
                   content
                       .offset(x:value.xTranslation,y: value.yTranslation)
               } keyframes : { _ in
                   KeyframeTrack(\.xTranslation) {
                       CubicKeyframe(0, duration: 3)
                       CubicKeyframe(-1*Double.random(in:(75..<150)), duration: 4)
                       CubicKeyframe(Double.random(in:(75..<150)), duration: 4)
                       CubicKeyframe(0, duration: 5)
                   }
                   KeyframeTrack(\.yTranslation) {
                       CubicKeyframe(0, duration: 4)
                       CubicKeyframe(-1*Double.random(in:(75..<150)), duration: 6)
                       CubicKeyframe(-1*Double.random(in:(75..<150)), duration: 6)
                       CubicKeyframe(0, duration: 5)
                   }
               }
            
           Circle()
               .frame(width: 450)
               .foregroundStyle(.orange)
               .offset(x: 120, y: -200)
               .blur(radius: 180)
               .keyframeAnimator(initialValue: AnimationProperties(), repeating: true) { content, value in
                   content
                       .offset(x: value.xTranslation, y: value.yTranslation)
               } keyframes : { _ in
                   KeyframeTrack(\.xTranslation) {
                       CubicKeyframe(Double.random(in:(150..<250)), duration: 6)
                       CubicKeyframe(0, duration: 6)
                   }
                   KeyframeTrack(\.yTranslation) {
                       CubicKeyframe(Double.random(in:(150..<250)), duration: 6)
                       CubicKeyframe(0, duration: 6)
                   }
               }
        }
    }
}
