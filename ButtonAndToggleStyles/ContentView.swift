//
//  ContentView.swift
//  ButtonAndToggleStyles
//
//  Created by vikingosegundo on 18.12.23.
//

import SwiftUI

struct ContentView: View {

    var body: some View {
        TabView {
            GlassmorphismExamples()
                .tabItem { Label("Glassmorphism", systemImage: "list.dash") }
            NeumorphismExamples()
                .tabItem { Label("Neumorphism", systemImage: "list.dash") }
            NeubrutalismExamples()
                .tabItem { Label("Neubrutalism", systemImage: "list.dash") }
        }
    }
}

#Preview {
    ContentView()
}
